package partiti.iscritti;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang3.tuple.Pair;

import partiti.Partito;

public class ProcesserPrinter {

	public static final int STARTING_YEAR = 1990;
	public static final int ENDING_YEAR = 2018;
	
	public static void main(String[] args) throws IOException {
//		 processPCI();
//		 processPD();
//		 processM5S();
//		 processFI();
		 processSI();
//		 processLN();
	}

	private static String processPCI() throws IOException {
		Partito partito = new Partito("src/main/resources/csv/PCI.csv", "PCI", 1945, 1990);
		String json = partito.getJson(STARTING_YEAR, ENDING_YEAR);
		System.out.println(json);
		return json;
	}

	private static String processPD() throws IOException {
		Partito partito = new Partito("src/main/resources/csv/PD.csv", "PD", 2007, 2017);
		String json = partito.getJson(STARTING_YEAR, ENDING_YEAR);
		System.out.println(json);
		return json;
	}

	private static String processM5S() throws IOException {
		Partito partito = new Partito("src/main/resources/csv/M5S.csv", "M5S", 2009, 2017);
		String json = partito.getJson(STARTING_YEAR, ENDING_YEAR);
		System.out.println(json);
		return json;
	}

	private static String processFI() throws IOException {
		String json;
		{
			Partito partito = new Partito("src/main/resources/csv/FI2013.csv", "FI", 2013, 2017);
			json = partito.getJson(STARTING_YEAR, ENDING_YEAR);
			System.out.println(json);
		}
		{
			Partito partito = new Partito("src/main/resources/csv/FI1994.csv", "FI", 1994, 2009);
			String json2 = partito.getJson(STARTING_YEAR, ENDING_YEAR);
			System.out.println(json2);
			return json+json2;
		}
	}

	private static String processSI() throws IOException {
		Partito partito = new Partito("src/main/resources/csv/SI.csv", "SI", 2017, 2017);
		String json = partito.getJson(STARTING_YEAR, ENDING_YEAR);
		System.out.println(json);
		return json;
	}

	private static String processLN() throws IOException {
		Partito partito = new Partito("src/main/resources/csv/LN.csv", "LN", 1989, 2017);
		String json = partito.getJson(STARTING_YEAR, ENDING_YEAR);
		System.out.println(json);
		return json;
	}

}

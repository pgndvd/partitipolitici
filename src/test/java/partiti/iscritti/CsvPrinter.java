package partiti.iscritti;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang3.tuple.Pair;

public class CsvPrinter {
	
	public static void main(String[] args) throws IOException {
//		printPCI();
//		printPD();
//		printM5S();
//		printFI();
//		printSI();
		printLN();
	}



	private static void printLN() {
		for(int anno = 1945; anno < 1992; anno++){
			printRow("LN", anno, 1, null);
		}
		//https://it.wikipedia.org/w/index.php?title=Lega_Nord#Iscritti		
		List<Pair<String, String>> list = Arrays.asList(
				Pair.of("1992","112.400"),
				Pair.of("1993","147.297"),
				Pair.of("1994","167.650"),
				Pair.of("1995","123.031"),
				Pair.of("1996","112.970"),
				Pair.of("1997","136.503"),
				Pair.of("1998","121.777"),
				Pair.of("1999","123.352"),
				Pair.of("2000","120.897"),
				Pair.of("2001","124.310"),
				Pair.of("2002","119.753"),
				Pair.of("2003","131.423"),
				Pair.of("2004","1"),
				Pair.of("2005" ,"1"),
				Pair.of("2006" ,"1"),
				Pair.of("2007" ,"1"),
				Pair.of("2008" ,"1"),
				Pair.of("2009" ,"1"),
				Pair.of("2010","182.502"),
				Pair.of("2011" ,"1"),
				Pair.of("2012" ,"1"),
				Pair.of("2013","122.000"),
				Pair.of("2014","1"),
				Pair.of("2015","1"),
				Pair.of("2016","1")
		);

		for(Pair<String, String> pair: list){
			int anno = Integer.parseInt(pair.getKey());
			String value =  pair.getValue().replace(".", "");
			printRow("LN", anno, value, "https://it.wikipedia.org/w/index.php?title=Lega_Nord#Iscritti");
		}

	}

	private static void printSI() {
		for(int anno = 1945; anno < 2016; anno++){
			printRow("FI", anno, 1, null);
		}
		//https://it.wikipedia.org/wiki/Sinistra_Italiana
		printRow("FI", 2017, 19346, "https://it.wikipedia.org/wiki/Sinistra_Italiana"); //19.346[1] (2017)
	}

	private static void printFI() {
		for(int anno = 1945; anno < 2014; anno++){
			printRow("FI", anno, 1, null);
		}
		
		//https://it.wikipedia.org/wiki/Forza_Italia_(1994)#Iscritti
		List<Pair<String, String>> list = Arrays.asList(
				Pair.of("1995","5.2001"),
				Pair.of("1996","1"),
				Pair.of("1997","139.546"),
				Pair.of("1998","161.319"),
				Pair.of("1999","190.398"),
				Pair.of("2000","312.863"),
				Pair.of("2001","271.751"),
				Pair.of("2002","222.631"),
				Pair.of("2003","249.824"),
				Pair.of("2004","1"),
				Pair.of("2005","1"),
				Pair.of("2006","1"),
				Pair.of("2007","401.014")
		);
		
		for(Pair<String, String> pair: list){
			int anno = Integer.parseInt(pair.getKey());
			String value =  pair.getValue().replace(".", "");
			printRow("FI", anno, value, "https://it.wikipedia.org/wiki/Forza_Italia_(1994)#Iscritti");
		}

		//https://it.wikipedia.org/w/index.php?title=Forza_Italia_(2013)&oldid=68518407
		printRow("FI", 2014, 8300, "https://it.wikipedia.org/w/index.php?title=Forza_Italia_(2013)");
		//https://en.wikipedia.org/wiki/Forza_Italia_(2013)#cite_note-2
		printRow("FI", 2015, 106000, "https://en.wikipedia.org/wiki/Forza_Italia_(2013)");
		//https://it.wikipedia.org/wiki/Forza_Italia_(2013)
		printRow("FI", 2016, 165000, "https://it.wikipedia.org/wiki/Forza_Italia_(2013)");
	}

	private static void printM5S() {
		for(int anno = 1945; anno < 2012; anno++){
			printRow("M5S", anno, 1, null);
		}
		//https://it.wikipedia.org/w/index.php?title=Movimento_5_Stelle&oldid=63552860
		printRow("M5S", 2012, 255339, "https://it.wikipedia.org/w/index.php?title=Movimento_5_Stelle&oldid=63552860"); //255 339[13] (2012)
		//https://it.wikipedia.org/w/index.php?title=Movimento_5_Stelle&oldid=63552860
		printRow("M5S", 2013, 80383, "https://it.wikipedia.org/w/index.php?title=Movimento_5_Stelle&oldid=63552860"); //80.383[16] (30 giugno 2013)
		//https://it.wikipedia.org/w/index.php?title=Movimento_5_Stelle&oldid=83986359
		printRow("M5S", 2014, 87656, "https://it.wikipedia.org/w/index.php?title=Movimento_5_Stelle&oldid=83986359"); //87.656[15] (1° gennaio 2014)
		//https://it.wikipedia.org/w/index.php?title=Movimento_5_Stelle&oldid=75823007
		printRow("M5S", 2015, 130000, "https://it.wikipedia.org/w/index.php?title=Movimento_5_Stelle&oldid=75823007"); //130 000 (17 ottobre 2015)
		//https://it.wikipedia.org/wiki/Movimento_5_Stelle#cite_note-13
		printRow("M5S", 2016, 135023, "https://it.wikipedia.org/wiki/Movimento_5_Stelle");
	}

	private static void printPD() {
		//https://it.wikipedia.org/wiki/Partito_Democratico_(Italia)#Iscritti
		List<Pair<String, String>> list = Arrays.asList(
				Pair.of("2009","831 042"),
				Pair.of("2010","617 240"),
				Pair.of("2011","607 897"),
				Pair.of("2012","500 163"),
				Pair.of("2013","539 354"),
				Pair.of("2014","378 187"),
				Pair.of("2015","395 320"),
				Pair.of("2016","405 041"));
		
		for(int anno = 1945; anno < 2009; anno++){
			printRow("PD", anno, 1, null);
		}
		
		for(Pair<String, String> pair: list){
			int anno = Integer.parseInt(pair.getKey());
			String value =  pair.getValue().replace(" ", "");
			printRow("PD", anno, value, "https://it.wikipedia.org/wiki/Partito_Democratico_(Italia)#Iscritti");
		}
	}

	private static void printPCI() {
		//http://partitocomunistaitaliano.blogspot.com/2006/09/gli-iscritti-al-pci.html
		List<Pair<String, String>> list = Arrays.asList(
				Pair.of("1945","1.770.896"),
				Pair.of("1946","2.068.272"),
				Pair.of("1947","2.252.446"),
				Pair.of("1948","2.115.232"),
				Pair.of("1949","2.027.271"),
				Pair.of("1950","2.112.593"),
				Pair.of("1951","2.097.830"),
				Pair.of("1952","2.093.540"),
				Pair.of("1953","2.134.285"),
				Pair.of("1954","2.145.317"),
				Pair.of("1955","2.090.006"),
				Pair.of("1956","2.035.353"),
				Pair.of("1957","1.825.342"),
				Pair.of("1958","1.818.606"),
				Pair.of("1959","1.789.269"),
				Pair.of("1960","1.792.974"),
				Pair.of("1961","1.728.620"),
				Pair.of("1962","1.630.550"),
				Pair.of("1963","1.615.571"),
				Pair.of("1964","1.641.214"),
				Pair.of("1965","1.615.296"),
				Pair.of("1966","1.575.935"),
				Pair.of("1967","1.534.705"),
				Pair.of("1968","1.502.862"),
				Pair.of("1969","1.503.816"),
				Pair.of("1970","1.507.047"),
				Pair.of("1971","1.521.642"),
				Pair.of("1972","1.584.659"),
				Pair.of("1973","1.623.082"),
				Pair.of("1974","1.657.825"),
				Pair.of("1975","1.730.453"),
				Pair.of("1976","1.814.262"),
				Pair.of("1977","1.814.154"),
				Pair.of("1978","1.790.450"),
				Pair.of("1979","1.761.297"),
				Pair.of("1980","1.751.323"),
				Pair.of("1981","1.714.052"),
				Pair.of("1982","1.673.751"),
				Pair.of("1983","1.635.264"),
				Pair.of("1984","1.619.940"),
				Pair.of("1985","1.595.281"),
				Pair.of("1986","1.551.576"),
				Pair.of("1987","1.508.140"),
				Pair.of("1988","1.462.281"),
				Pair.of("1989","1.421.230"),
				Pair.of("1990","1.264.790")
				);
		
		for(Pair<String, String> pair: list){
			int anno = Integer.parseInt(pair.getKey());
			String value =  pair.getValue().replace(".", "");
			printRow("PCI", anno, value, "http://partitocomunistaitaliano.blogspot.com/2006/09/gli-iscritti-al-pci.html");
		}
		
		for(int anno = 1991; anno < 2017; anno++){
			printRow("PCI", anno, 1, "http://partitocomunistaitaliano.blogspot.com/2006/09/gli-iscritti-al-pci.html");
		}

	}

	private static void printRow(String partito, int anno, Object value, String source) {
		//                      [ 'M5S(2010)', 'M5S(2011)', 607897 ],
		//System.out.println("['"+partito+"("+(anno-1)+")', '"+partito+"("+(anno)+")', "+value+" ],");
		
		System.out.println(anno+","+value+","+"false,"+source);
	}
}

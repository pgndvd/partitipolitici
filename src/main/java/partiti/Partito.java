package partiti;

import java.io.FileReader;
import java.io.IOException;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import com.opencsv.CSVReader;

public class Partito {

	public final static int DEFAULT_VALUE = 1;

	public String csvFile;
	public String printedName;

	public int annoFondazione;
	public int annoScioglimento;

	public Partito(String csvFile, String printedName) throws IOException {
		this.csvFile = csvFile;
		this.printedName = printedName;
		this.annoFondazione = calcolaAnnoFondazione(csvFile);
		this.annoScioglimento = calcolaAnnoScioglimento(csvFile);
	}

	private int calcolaAnnoScioglimento(String csvFile) throws IOException {
		CSVReader reader = new CSVReader(new FileReader(csvFile));
		List<String[]> list = reader.readAll();
		sortListByYear(list);
		reader.close();
		return  Integer.parseInt(list.get(list.size()-1)[0]);
	}

	private int calcolaAnnoFondazione(String csvFile) throws IOException {
		CSVReader reader = new CSVReader(new FileReader(csvFile));
		List<String[]> list = reader.readAll();
		sortListByYear(list);
		reader.close();
		return  Integer.parseInt(list.get(0)[0]);
	}

	public Partito(String csvFile, String printedName, int annoFondazione, int annoScioglimento) {
		this.csvFile = csvFile;
		this.printedName = printedName;
		this.annoFondazione = annoFondazione;
		this.annoScioglimento = annoScioglimento;
	}

	public String getJson(int annoInizio, int annoFine) throws IOException {
		if (annoInizio > annoScioglimento || annoFine < annoFondazione) {
			return "";
		}
		StringBuilder builder = new StringBuilder();
		CSVReader reader = new CSVReader(new FileReader(csvFile));

		int currentYear = annoInizio;

		// Riempie gli anni prima della fondazione
		while (currentYear < annoFondazione && currentYear < annoFine) {
			// System.out.println("CurrentYear prima fondazione " +
			// currentYear);
			builder.append(newRow(currentYear, DEFAULT_VALUE, false));
			currentYear++;
		}

		List<String[]> list = reader.readAll();
		sortListByYear(list);
		
		String[] lastLine = null;
		for(int i = 0; i<list.size()  && currentYear < annoFine; i++){
			String[] line = list.get(i);
			Integer anno = Integer.parseInt(line[0]);

			if (anno < annoInizio) {
				continue;
			}

			while (anno != currentYear && currentYear < annoScioglimento) {
				System.out.println("Interpolate " + printedName + " " + currentYear);
				int interpolatedValue = DEFAULT_VALUE; // TODO
				builder.append(newRow(currentYear, interpolatedValue, true));
				currentYear++;
			}

			String value = line[1];
			builder.append(newRow(anno, value, false));

			// System.out.println("CurrentYear partito " + currentYear);
			currentYear = anno + 1;
			lastLine = line;
		}
		reader.close();

		// Interpolazione anni dopo lo scioglimento
		while (currentYear < annoScioglimento && currentYear < annoFine) {
			builder.append(newRow(currentYear, lastLine == null ? DEFAULT_VALUE : lastLine[1], true));
			currentYear++;
		}

		// Riempie gli anni dopo lo scioglimento
		while (currentYear < annoFine) {
			// System.out.println("CurrentYear dopo scioglimento " +
			// currentYear);
			builder.append(newRow(currentYear, DEFAULT_VALUE, false));
			currentYear++;
		}

		return builder.toString();
	}

	private String newRow(int year, Object value, boolean interpolated) {
		String row = "['" //
				+ printedName.replace('\'', ' ') + "(" + (year) + ")', '" //
				+ printedName.replace('\'', ' ') + "(" + (year + 1) + ")', " //
				+ value + "," //
				+ (interpolated ? 0 : 1) //
				// +"," +getStyle()
				+ " ],\n";
		// System.out.println(row);
		return row;
	}

	private String getStyle() {
		return "{style: 'font-style:bold; font-size:22px;'}";
	}

	private void sortListByYear(List<String[]> list) {
		Collections.sort(list, new Comparator<String[]>() {
			@Override
			public int compare(String[] o1, String[] o2) {
				int anno1 = Integer.parseInt(o1[0]);
				int anno2 = Integer.parseInt(o2[0]);
				return Integer.compare(anno1, anno2);
			}
		});
	}

}


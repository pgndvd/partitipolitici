package partiti.iscritti;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.List;

import com.github.jknack.handlebars.Context;
import com.github.jknack.handlebars.Handlebars;
import com.github.jknack.handlebars.Template;

import partiti.Partito;

public class Processer {

	public static final int STARTING_YEAR = 2010;
	public static final int ENDING_YEAR = 2018;

	public static final String BASE_FOLDER = "src/main/resources/iscritti/csv/";
	public static void main(String[] args) throws IOException, URISyntaxException {
		List<Partito> partiti = Arrays.asList(//
				new Partito(BASE_FOLDER+"PCI.csv", "PCI", 1945, 1990), //
				new Partito(BASE_FOLDER+"PD.csv", "PD", 2007, 2018), //
				new Partito(BASE_FOLDER+"M5S.csv", "M5S", 2009, 2018), //
				new Partito(BASE_FOLDER+"SI.csv", "SI", 2017, 2018), //
				new Partito(BASE_FOLDER+"LN.csv", "LN", 1989, 2018));

		StringBuilder finalJson = new StringBuilder();
		for (Partito partito : partiti) {
			finalJson.append(partito.getJson(STARTING_YEAR, ENDING_YEAR));
		}
		
		finalJson.append(new Partito(BASE_FOLDER+"FI1994.csv", "FI", 1994, 2009).getJson(STARTING_YEAR, 2010)); 
		finalJson.append(new Partito(BASE_FOLDER+"FI2013.csv", "FI", 2013, 2018).getJson(2010, ENDING_YEAR));
		
		System.out.println(finalJson);

		Handlebars handlebars = new Handlebars();

		Template template = handlebars.compile("iscritti/index");

		Context context = Context.newContext(finalJson.toString()).combine("title", "Iscritti");

		File outputHtml = new File("src/main/resources/iscritti.html");
		Writer writer = new FileWriter(outputHtml);
		template.apply(context, writer);
		writer.flush();
		writer.close();

		java.awt.Desktop.getDesktop().browse(new URI("file://"+outputHtml.getAbsolutePath()));

	}
}

package partiti.senato;

import java.util.Arrays;
import java.util.List;

public class PartitiEnumGenerator {

	public static void main(String[] args) {
		List<String> nomiPartito = Arrays.asList(
				"Sinistra Democratica", //
				"Sinistra Democratica per il Socialismo Europeo", //
				"Verdi - La Rete", //
				"Grandi Autonomie e Libertà (Grande Sud, Popolari per l'Italia, Moderati, Idea, Euro-Exit, M.P.L. - Movimento politico Libertas, Riscossa Italia)", //
				"Movimento Sociale Italiano", //
				"Scudo Crociato", //
				"Movimento 5 Stelle", //
				"Insieme con l'Unione Verdi - Comunisti Italiani", //
				"Rifondazione Comunista - Sinistra Europea", //
				"Verdi - l'Ulivo", //
				"Rifondazione Comunista", //
				"Italia dei Valori", //
				"Lega Nord", //
				"Forza Italia", //
				"Lega", //
				"L'Ulivo", //
				"PSI-PSDI unificati", //
				"Rinnovamento Italiano", //
				"Sinistra Democratica - l'Ulivo", //
				"Progressisti - Federativo", //
				"Rifondazione Comunista - Progressisti", //
				"Misto", //
				"Democratico Indipendenti di Sinistra", //
				"Democratico Cristiano", //
				"Margherita", //
				"Partito Nazionale Monarchico", //
				"Federazione Cristiano Democratica - CDU", //
				"Comunista", //
				"Movimento Sociale Italiano - Destra Nazionale", //
				"Alleanza Nazionale - Movimento Sociale Italiano", //
				"UDC, SVP e Autonomie", //
				"Libero - Social - Repubblicano", //
				"Democratici di Sinistra - l'Ulivo", //
				"Per le Autonomie", //
				"Socialdemocratico - Liberale", //
				"Lega Nord e Autonomie", //
				"Partito Democratico", //
				"Partito Socialista Italiano", //
				"Futuro e Libertà per l'Italia", //
				"Rinnov. Ital. Liberaldem. Ind.-Pop. per l'Europa", //
				"Coesione Nazionale", //
				"Partito Socialista Democratico Italiano", //
				"Federalista Europeo Ecologista", //
				"Centro Cristiano Democratico", //
				"Partito Popolare Italiano", //
				"Lega Nord Padania", //
				"Lega Federalista Italiana", //
				"Democrazia Cristiana - Indipendenti - Movimento per l'Autonomia", //
				"Liberale", //
				"Partito Liberale Italiano", //
				"Democrazia Europea", //
				"Movimento Sociale Italiano e Part. Naz. Monarchico", //
				"Repubblicano", //
				"Democratico di Sinistra", //
				"Alleanza Nazionale", //
				"Federazione Cristiano Democratica - CCD", //
				"Per il Terzo Polo (ApI-FLI)", //
				"Alternativa Popolare - Centristi per l'Europa", //
				"CCD-CDU: Biancofiore", //
				"Unione dei Democraticicristiani e di Centro (UDC)", //
				"Centrodestra Nazionale", //
				"Partito Socialista Unitario", //
				"Rinnovamento Italiano e Indipendenti", //
				"Democrazia Nazionale - Costituente di Destra", //
				"Per le Autonomie (SVP-UV-PATT-UPT)-PSI-MAIE", //
				"Forza Italia-Il Popolo della Libertà XVII Legislatura", //
				"Partito Socialista Italiano di Unita' Proletaria", //
				"ALA - Scelta Civica per la Costituente Liberale e Popolare", //
				"Il Popolo della Libertà", //
				"Progressisti-Verdi-La Rete", //
				"Partito Democratico della Sinistra", //
				"Sinistra Indipendente", //
				"Unita' Socialista", //
				"Progressista - PSI", //
				"Articolo 1 - Movimento democratico e progressista"
				);
		
		for(String nomePartito : nomiPartito){
			
		}
	}
}

package partiti.senato;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.tuple.Pair;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import partiti.Partito;
import utils.ParsingUtil;

public class SenatoItParser {
	private static Map<String, List<String>> namesMap = new HashMap<>();

	private static Map<String, Map<Integer, AtomicLong>> seggiMap = new ConcurrentHashMap<>();

	private static boolean writeFile = false;
	
	public static void main(String[] args) throws IOException {
		for (int legislatura = 1; legislatura < 18; legislatura++) {
			parseLegislatura(legislatura);
			System.out.println(seggiMap);
		}

		for (Entry<String, Map<Integer, AtomicLong>> entry : seggiMap.entrySet()) {
			String partito = entry.getKey();
			Map<Integer, AtomicLong> seggiAnnuali = entry.getValue();
			for (Entry<Integer, AtomicLong> seggioAnnuale : seggiAnnuali.entrySet()) {
				int anno = seggioAnnuale.getKey();
				long seggi = seggioAnnuale.getValue().get();
				print(anno, partito, seggi);
			}
		}
		
		for (Entry<String, Map<Integer, AtomicLong>> entry : seggiMap.entrySet()) {
			String partito = entry.getKey();
			System.out.println("new Partito(BASE_FOLDER+\""+partitoNameToCsvName(partito)+"\", \""+partito+"\", STARTING_YEAR, ENDING_YEAR), //");
		}
		
		// parseLegislatura(2);
		// System.out.println(seggiMap);

	}

	private static void parseLegislatura(int legislatura) throws IOException {
		String url = "http://www.senato.it/loc/link.asp?tipodoc=GRPEL&leg=" + legislatura;
		String content = ParsingUtil.getSourceAsString(url);

		Document doc = Jsoup.parse(content);

		Elements tables = doc.getElementsByTag("table");

		for (Element table : tables) {
			for (Element tr : table.getElementsByTag("tr")) {
				Element partito = tr.children().first();
				Element href = partito.children().first();
				String partitoUrl = href.attr("href");
				String nomePartito = href.text();
				if (!nomePartito.equals("Totale")) {
					parsePartito(legislatura, nomePartito, partitoUrl);
				}
				String seggi = tr.children().last().text();
				// print(legislatura, nomePartito, seggi);
			}
		}
	}

	private static void parsePartito(int legislatura, String nomePartito, String partitoUrl) throws IOException {
		String baseUrl = "http://www.senato.it";
		String content = ParsingUtil.getSourceAsString(fixRedirect(baseUrl + partitoUrl));

		Document doc = Jsoup.parse(content);

		Elements tables = doc.getElementsByTag("table");

		if (tables.size() != 1) {
			throw new RuntimeException("Too many tables in " + partitoUrl + " " + nomePartito);
		}

		int inizioAnno = legislaturaToYear(legislatura);
		int fineAnno = legislaturaToYear(legislatura + 1);

		for (Element table : tables) {
			for (Element tr : table.getElementsByTag("tr")) {
				List<String> notCandidates = Arrays.asList(//
						"Presidente", //
						"Presidenti pro tempore", //
						"Vicepresidenti", //
						"Segretari Amministrativi", //
						"Comitato Direttivo", //
						"Segretari", //
						"Vicesegretari", //
						"Vicepresidenti Vicari", //
						"Revisori dei conti", //
						"Sindaci revisori", //
						"Tesorieri", //
						"Vicepresidenti Tesorieri", //
						"Segretari d'Aula", //
						"Responsabili dell'Amministrazione", //
						"Organizzazione", //
						"Membri");
				if (notCandidates.contains(tr.text()) && tr.toString().contains("<strong>")) {
					continue;
				}
				if (tr.children().size() != 2) {
					System.out.println(tr);
					throw new RuntimeException("Too many children in " + partitoUrl);
				}
				String politico = tr.children().first().text();
				Element modifiers = tr.children().last();
				System.out.println(nomePartito + " "+politico);
				Pair<Integer, Integer> mandato = processModifiers(modifiers, inizioAnno, fineAnno,
						baseUrl + partitoUrl);
				int inizioMandato = mandato.getKey();
				int fineMandato = mandato.getValue();
				// System.out.println(politico +" dal "+ inizioMandato+"
				// al"+fineMandato);
				Map<Integer, AtomicLong> partitoMap = seggiMap.get(nomePartito);
				if (partitoMap == null) {
					partitoMap = new HashMap<>();
					seggiMap.put(nomePartito, partitoMap);
				}
				for (int anno = inizioMandato; anno < fineMandato; anno++) {
					AtomicLong seggi = partitoMap.get(anno);
					if (seggi == null) {
						seggi = new AtomicLong(1);
						partitoMap.put(anno, seggi);
					} else {
						seggi.incrementAndGet();
					}
					// System.out.println(nomePartito+" "+ anno+" "+seggi);
				}

			}
		}
	}

	private static String fixRedirect(String url) throws IOException {
		URL obj = new URL(url);
		HttpURLConnection.setFollowRedirects(false);
		HttpURLConnection conn = (HttpURLConnection) obj.openConnection();
		// for (Entry<String, List<String>> entry :
		// conn.getHeaderFields().entrySet()) {
		// System.out.println(entry);
		// }
		String newUrl = conn.getHeaderField("Location");

		HttpURLConnection.setFollowRedirects(true);
		newUrl = newUrl.replace("/Gruppi/", "/GruppiStorici/");

		HttpURLConnection.setFollowRedirects(true);

		return newUrl == null ? url : newUrl;
	}

	private static Pair<Integer, Integer> processModifiers(Element modifiers, int inizioAnno, int fineAnno,
			String url) {
		int inizioMandato = inizioAnno;
		int fineMandato = fineAnno;
		if (modifiers.text().length() > 0) {
			String[] split = modifiers.toString().split("<br>");
			if (split.length == 0) {
				throw new RuntimeException("Wtf");
			}
			if (split.length > 3) {
				System.err.println("Interesting: " + modifiers);
			}
			for (String modifier : split) {
				if (modifier.equals(" </td>")) {
					continue;
				}
				int matches = 0;
//				System.out.println(url);
//				System.out.println(modifier);
//				System.out.println(modifiers);
				if (modifier.startsWith(" il") || modifier.contains("> il")) {
					modifier = modifier.replace(" il", "Fino al");
				}
				if (modifier.startsWith(" l'") || modifier.contains("> l'")) {
					modifier = modifier.replace(" l'", "Fino al ");
				}
				{
					modifier = modifier.replace("Deceduto il", "Fino al");
					modifier = modifier.replace("Deceduta il", "Fino al");
					modifier = modifier.replace("Deceduto l'", "Fino al ");
					modifier = modifier.replace("Esce dal gruppo il", "Fino al");
					modifier = modifier.replace("Esce dal gruppo l'", "Fino al ");
					modifier = modifier.replace("Dimissioni accettate il", "Fino al");
					modifier = modifier.replace("Dimissioni accettate l'", "Fino al ");
					modifier = modifier.replace("Dimessosi per incompatibilità il", "Fino al");
					modifier = modifier.replace("Dimessasi per incompatibilità il", "Fino al");
					//@formatter:off
					// modifier = modifier.replace("Comitato Direttivo del gruppo dal", "Fino al");
					// modifier = modifier.replace("Vicesegretario del gruppo dal", "Fino al");
					// modifier = modifier.replace("Vicepresidente del gruppo dal", "Fino al");
					// modifier = modifier.replace("Presidente del gruppo dal","Fino al");
					// modifier = modifier.replace("Segretario del gruppo dal","Fino al");
					// modifier = modifier.replace("Segretario Amministrativo del gruppo dal", "Fino al");
					// modifier = modifier.replace("Revisore dei conti del gruppo dal", "Fino al");
					// modifier = modifier.replace("Tesoriere del gruppo dal","Fino al");
					// modifier = modifier.replace("Vicepresidente Tesoriere del gruppo dal", "Fino al");
					// modifier = modifier.replace("Vicepresidente Vicario del gruppo dal", "Fino al");
					// modifier = modifier.replace("Segretario d'Aula del gruppo dal", "Fino al");
					//@formatter:on
					modifier = modifier.replace("del gruppo dal", "Fino al");
					modifier = modifier.replace("Eletto membro della Corte Costituzionale il", "Fino al");
					modifier = modifier.replace("Eletto presidente della Repubblica il", "Fino al");
					// ehehehe grande berlu
					modifier = modifier.replace("Mancata convalida dell'elezione il", "Fino al");
					String pattern = "Fino al(l'| )\\d\\d? [a-z]+ (\\d\\d\\d\\d)";
					Pattern r = Pattern.compile(pattern);
					Matcher m = r.matcher(modifier);
					while (m.find()) {
						matches++;
						// System.out.println("Fino al: " + m.group(2));
						fineMandato = Integer.parseInt(m.group(2));
					}
				}
				{
					modifier = modifier.replace("Entra nel gruppo il", "Dal");
					modifier = modifier.replace("Entra nel gruppo l'", "Dal ");
					modifier = modifier.replace("Dall'ingresso nel gruppo il", "Dal");
					modifier = modifier.replace("Dall'ingresso nel gruppo l'", "Dal ");
					modifier = modifier.replace("Dall'", "Dal ");
					String pattern = "Dal \\d\\d? [a-z]+ (\\d\\d\\d\\d)";
					Pattern r = Pattern.compile(pattern);
					Matcher m = r.matcher(modifier);
					while (m.find()) {
						matches++;
						// System.out.println("Dal: " + m.group(1));
						inizioMandato = Integer.parseInt(m.group(1));
					}
				}
				{
					String pattern = "Annullata l'elezione il \\d\\d? [a-z]+ (\\d\\d\\d\\d)";
					Pattern r = Pattern.compile(pattern);
					Matcher m = r.matcher(modifier);
					while (m.find()) {
						matches++;
						System.out.println("ANNULLATO: " + m.group(1) + " " + url);
						fineMandato = Integer.parseInt(m.group(1));
					}
				}
				if (matches == 0) {
					throw new RuntimeException("Unrecognized modifier : " + modifiers + " " + url);
				}
			}
		}
		return Pair.of(inizioMandato, fineMandato);
	}

	private static void print(int anno, String partito, long seggi) throws IOException {
		String filename = partitoNameToCsvName(partito);
		
		if(writeFile){
		File outputCsv = new File("src/main/resources/senato/csv/" + filename + ".csv");
		System.out.println(outputCsv.getAbsolutePath());
		FileWriter writer = new FileWriter(outputCsv, true);
		writer.write(anno + "," + seggi + ",false,\"" + partito + "\"\n");
		writer.flush();
		writer.close();
		}
	}

	private static String partitoNameToCsvName(String partito) {
		String filename = partito.toLowerCase().replaceAll("\\(.*\\)", "").replaceAll("[^a-z]", "");

		//@formatter:off
		//Smart grouping of files (maybe too smart)
		//		if (namesMap.get(filename) == null) {
		//			boolean found = false;
		//			for (Entry<String, List<String>> entry : namesMap.entrySet()) {
		//				for (String name : entry.getValue()) {
		//					if (name.contains(filename) || filename.contains(name)) {
		//						entry.getValue().add(filename);
		//						filename = name;
		//						found = true;
		//						break;
		//					}
		//				}
		//			}
		//			if (!found) {
		//				namesMap.put(filename, new ArrayList<>(Arrays.asList(filename)));
		//			}
		//		}
		//@formatter:on
		
		return filename;
	}

	public static int legislaturaToYear(int legislatura) {
		Map<Integer, Integer> map = new HashMap<>();
		map.put(1, 1948);
		map.put(2, 1953);
		map.put(3, 1958);
		map.put(4, 1963);
		map.put(5, 1968);
		map.put(6, 1972);
		map.put(7, 1976);
		map.put(8, 1979);
		map.put(9, 1983);
		map.put(10, 1987);
		map.put(11, 1992);
		map.put(12, 1994);
		map.put(13, 1996);
		map.put(14, 2001);
		map.put(15, 2006);
		map.put(16, 2008);
		map.put(17, 2013);
		map.put(18, 2018);

		return map.get(legislatura);
	}
}

package partiti.senato;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import com.github.jknack.handlebars.Context;
import com.github.jknack.handlebars.Handlebars;
import com.github.jknack.handlebars.Template;

import partiti.Partito;

public class Processer {

	public static final int STARTING_YEAR = 1964;
	public static final int ENDING_YEAR = 1980;

	public static final String BASE_FOLDER = "src/main/resources/senato/csv/";

	public static void main(String[] args) throws IOException, URISyntaxException {
		List<Partito> partiti = Arrays.asList(//
				new Partito(BASE_FOLDER + "sinistrademocratica.csv", "Sinistra Democratica"), //
				new Partito(BASE_FOLDER + "sinistrademocraticaperilsocialismoeuropeo.csv",
						"Sinistra Democratica per il Socialismo Europeo"), //
				new Partito(BASE_FOLDER + "verdilarete.csv", "Verdi - La Rete"), //
				new Partito(BASE_FOLDER + "grandiautonomieelibert.csv",
						//"Grandi Autonomie e Libertà (Grande Sud, Popolari per l'Italia, Moderati, Idea, Euro-Exit, M.P.L. - Movimento politico Libertas, Riscossa Italia)"), //
						"GAL"),
				new Partito(BASE_FOLDER + "movimentosocialeitaliano.csv", "Movimento Sociale Italiano"), //
				new Partito(BASE_FOLDER + "scudocrociato.csv", "Scudo Crociato"), //
				new Partito(BASE_FOLDER + "movimentostelle.csv", "Movimento 5 Stelle"), //
				new Partito(BASE_FOLDER + "insiemeconlunioneverdicomunistiitaliani.csv",
						"Insieme con l'Unione Verdi - Comunisti Italiani"), //
				new Partito(BASE_FOLDER + "rifondazionecomunistasinistraeuropea.csv",
						"Rifondazione Comunista - Sinistra Europea"), //
				new Partito(BASE_FOLDER + "verdilulivo.csv", "Verdi - l'Ulivo"), //
				new Partito(BASE_FOLDER + "rifondazionecomunista.csv", "Rifondazione Comunista"), //
				new Partito(BASE_FOLDER + "italiadeivalori.csv", "Italia dei Valori"), //
				new Partito(BASE_FOLDER + "leganord.csv", "Lega Nord"), //
				new Partito(BASE_FOLDER + "forzaitalia.csv", "Forza Italia"), //
				new Partito(BASE_FOLDER + "lega.csv", "Lega"), //
				new Partito(BASE_FOLDER + "lulivo.csv", "L'Ulivo"), //
				new Partito(BASE_FOLDER + "psipsdiunificati.csv", "PSI-PSDI unificati"), //
				new Partito(BASE_FOLDER + "rinnovamentoitaliano.csv", "Rinnovamento Italiano"), //
				new Partito(BASE_FOLDER + "sinistrademocraticalulivo.csv", "Sinistra Democratica - l'Ulivo"), //
				new Partito(BASE_FOLDER + "progressistifederativo.csv", "Progressisti - Federativo"), //
				new Partito(BASE_FOLDER + "rifondazionecomunistaprogressisti.csv",
						"Rifondazione Comunista - Progressisti"), //
				new Partito(BASE_FOLDER + "misto.csv", "Misto"), //
				new Partito(BASE_FOLDER + "democraticoindipendentidisinistra.csv",
						"Democratico Indipendenti di Sinistra"), //
				new Partito(BASE_FOLDER + "democraticocristiano.csv", "Democratico Cristiano"), //
				new Partito(BASE_FOLDER + "margherita.csv", "Margherita"), //
				new Partito(BASE_FOLDER + "partitonazionalemonarchico.csv", "Partito Nazionale Monarchico"), //
				new Partito(BASE_FOLDER + "federazionecristianodemocraticacdu.csv",
						"Federazione Cristiano Democratica - CDU"), //
				new Partito(BASE_FOLDER + "comunista.csv", "Comunista"), //
				new Partito(BASE_FOLDER + "movimentosocialeitalianodestranazionale.csv",
						"Movimento Sociale Italiano - Destra Nazionale"), //
				new Partito(BASE_FOLDER + "alleanzanazionalemovimentosocialeitaliano.csv",
						"Alleanza Nazionale - Movimento Sociale Italiano"), //
				new Partito(BASE_FOLDER + "udcsvpeautonomie.csv", "UDC, SVP e Autonomie"), //
				new Partito(BASE_FOLDER + "liberosocialrepubblicano.csv", "Libero - Social - Repubblicano"), //
				new Partito(BASE_FOLDER + "democraticidisinistralulivo.csv", "Democratici di Sinistra - l'Ulivo"), //
				new Partito(BASE_FOLDER + "perleautonomie.csv", "Per le Autonomie"), //
				new Partito(BASE_FOLDER + "socialdemocraticoliberale.csv", "Socialdemocratico - Liberale"), //
				new Partito(BASE_FOLDER + "leganordeautonomie.csv", "Lega Nord e Autonomie"), //
				new Partito(BASE_FOLDER + "partitodemocratico.csv", "Partito Democratico"), //
				new Partito(BASE_FOLDER + "partitosocialistaitaliano.csv", "Partito Socialista Italiano"), //
				new Partito(BASE_FOLDER + "futuroelibertperlitalia.csv", "Futuro e Libertà per l'Italia"), //
				new Partito(BASE_FOLDER + "rinnovitalliberaldemindpopperleuropa.csv",
						"Rinnov. Ital. Liberaldem. Ind.-Pop. per l'Europa"), //
				new Partito(BASE_FOLDER + "coesionenazionale.csv", "Coesione Nazionale"), //
				new Partito(BASE_FOLDER + "partitosocialistademocraticoitaliano.csv",
						"Partito Socialista Democratico Italiano"), //
				new Partito(BASE_FOLDER + "federalistaeuropeoecologista.csv", "Federalista Europeo Ecologista"), //
				new Partito(BASE_FOLDER + "centrocristianodemocratico.csv", "Centro Cristiano Democratico"), //
				new Partito(BASE_FOLDER + "partitopopolareitaliano.csv", "Partito Popolare Italiano"), //
				new Partito(BASE_FOLDER + "leganordpadania.csv", "Lega Nord Padania"), //
				new Partito(BASE_FOLDER + "legafederalistaitaliana.csv", "Lega Federalista Italiana"), //
				new Partito(BASE_FOLDER + "democraziacristianaindipendentimovimentoperlautonomia.csv",
						"Democrazia Cristiana - Indipendenti - Movimento per l'Autonomia"), //
				new Partito(BASE_FOLDER + "liberale.csv", "Liberale"), //
				new Partito(BASE_FOLDER + "partitoliberaleitaliano.csv", "Partito Liberale Italiano"), //
				new Partito(BASE_FOLDER + "democraziaeuropea.csv", "Democrazia Europea"), //
				new Partito(BASE_FOLDER + "movimentosocialeitalianoepartnazmonarchico.csv",
						"Movimento Sociale Italiano e Part. Naz. Monarchico"), //
				new Partito(BASE_FOLDER + "repubblicano.csv", "Repubblicano"), //
				new Partito(BASE_FOLDER + "democraticodisinistra.csv", "Democratico di Sinistra"), //
				new Partito(BASE_FOLDER + "alleanzanazionale.csv", "Alleanza Nazionale"), //
				new Partito(BASE_FOLDER + "federazionecristianodemocraticaccd.csv",
						"Federazione Cristiano Democratica - CCD"), //
				new Partito(BASE_FOLDER + "perilterzopolo.csv", "Per il Terzo Polo (ApI-FLI)"), //
				new Partito(BASE_FOLDER + "alternativapopolarecentristiperleuropa.csv",
						"Alternativa Popolare - Centristi per l'Europa"), //
				new Partito(BASE_FOLDER + "ccdcdubiancofiore.csv", "CCD-CDU: Biancofiore"), //
				new Partito(BASE_FOLDER + "unionedeidemocraticicristianiedicentro.csv",
						"Unione dei Democraticicristiani e di Centro (UDC)"), //
				new Partito(BASE_FOLDER + "centrodestranazionale.csv", "Centrodestra Nazionale"), //
				new Partito(BASE_FOLDER + "partitosocialistaunitario.csv", "Partito Socialista Unitario"), //
				new Partito(BASE_FOLDER + "rinnovamentoitalianoeindipendenti.csv",
						"Rinnovamento Italiano e Indipendenti"), //
				new Partito(BASE_FOLDER + "democrazianazionalecostituentedidestra.csv",
						"Democrazia Nazionale - Costituente di Destra"), //
				new Partito(BASE_FOLDER + "perleautonomiepsimaie.csv", "Per le Autonomie (SVP-UV-PATT-UPT)-PSI-MAIE"), //
				new Partito(BASE_FOLDER + "forzaitaliailpopolodellalibertxviilegislatura.csv",
						//"Forza Italia-Il Popolo della Libertà XVII Legislatura"), //
						"Il Popolo della Libertà"),
				new Partito(BASE_FOLDER + "partitosocialistaitalianodiunitaproletaria.csv",
						"Partito Socialista Italiano di Unita' Proletaria"), //
				new Partito(BASE_FOLDER + "alasceltacivicaperlacostituenteliberaleepopolare.csv",
						"ALA - Scelta Civica per la Costituente Liberale e Popolare"), //
				new Partito(BASE_FOLDER + "ilpopolodellalibert.csv", "Il Popolo della Libertà"), //
				new Partito(BASE_FOLDER + "progressistiverdilarete.csv", "Progressisti-Verdi-La Rete"), //
				new Partito(BASE_FOLDER + "partitodemocraticodellasinistra.csv", "Partito Democratico della Sinistra"), //
				new Partito(BASE_FOLDER + "sinistraindipendente.csv", "Sinistra Indipendente"), //
				new Partito(BASE_FOLDER + "unitasocialista.csv", "Unita' Socialista"), //
				new Partito(BASE_FOLDER + "progressistapsi.csv", "Progressista - PSI"), //
				new Partito(BASE_FOLDER + "articolomovimentodemocraticoeprogressista.csv",
						"Articolo 1 - Movimento democratico e progressista") //
		);
		StringBuilder finalJson = new StringBuilder();
		for (Partito partito : partiti) {
			finalJson.append(partito.getJson(STARTING_YEAR, ENDING_YEAR));
		}

		System.out.println(finalJson);

		Handlebars handlebars = new Handlebars();

		Template template = handlebars.compile("iscritti/index");

		int numberOfYears = ENDING_YEAR - STARTING_YEAR;
		int numberOfParties = StringUtils.countMatches(finalJson.toString(), "\n")/numberOfYears;
		Context context = Context.newContext(finalJson.toString()).combine("title", "Senato").combine("width", numberOfYears*250)
				.combine("height", numberOfParties*50);

		File outputHtml = new File("src/main/resources/senato.html");
		Writer writer = new FileWriter(outputHtml);
		template.apply(context, writer);
		writer.flush();
		writer.close();

		java.awt.Desktop.getDesktop().browse(new URI("file://" + outputHtml.getAbsolutePath()));

	}
}

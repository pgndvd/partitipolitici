package utils;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.Map.Entry;

import org.apache.commons.io.IOUtils;

public class ParsingUtil {

	public static String getSourceAsString(String spec) throws IOException {
		URL url = new URL(spec);

		URLConnection urlConn = url.openConnection();
		urlConn.setRequestProperty("User-Agent", "cheese");
		InputStream is = urlConn.getInputStream();
		
		String source = IOUtils.toString(is);
		return source;
	}

}
